
import os
import re
def get_file_prefix(file_path):
    
    file_name = os.path.basename(file_path)
    
    m = re.match("(.*)\.\d+\.(.*)", file_name)
    if m:
        return m.group(1) + ".d+." + m.group(2)
    
    # Normal seperator
    index = file_name.rfind('.')
    if index != -1:
        return file_name[:index]
    return file_name
    
