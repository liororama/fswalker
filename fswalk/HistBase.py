# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
HistBase Base Class for histograms
"""

import sys
import os
import time
import stat
import re
from operator import itemgetter
import abc
import string

import FSFileInfo

class HistBase:
    """HistBase Class"""

    __metaclass__ = abc.ABCMeta
     
    _verbose = 1
    _args = ""
    _err_msg = ""
    _sort_type = "name"

    _allowed_sorts = { "name" : "bucket_name",
                       "size" : "total_size_mb",
                       "files": "count"}

    _print_header = False
    _print_total = False
    _args_dict = {}
    
    _title = "AAAAA"
    _output_file = "/tmp/hist"    

    _generate_bar_chart = False
    _generate_pie_chart = False
    _generate_text = True
    _err_msg = ""
    _bucket_dict = {}    
    _bucket_name = ""
    _bucket_title = ""
    _bucket_order = None
    
    def __init__(self):
        """ Constructor """
        self._bucket_dict = {}
        
    def get_error(self):
        return self._err_msg
    
    def _set_err(self, err):
        self._err_msg = err
    
    def init(self, args):
        """Initialize the histogram"""
    @property
    def bucket_name(self):
        """ Bucket name property """
        return self._bucket_name
    
    @property
    def bucket_title(self):
        """ Bucket title property """
        return self._bucket_title
    
    @abc.abstractmethod    
    def add_file(self, file_info):
        """ Add a file to the histogram """
    
    
    def set_bucket_name_order(self, bucket_name_list):
        """ In cases when the buckets are known in advance (line in the size hist)
            it is necessary to set the order of the buckets in advance so when sorting
            the buckets by the bucket_name we get this predefined order """
        
        self._bucket_order = bucket_name_list    
        
    def save_to_file(self, file_name):
        """ Save the histogram to a file """

    def _prepare_args(self, args_str, seperator_re = "\s+"):
        args_parts = re.split(seperator_re, args_str)
        
        #print "PARTS:", args_parts
        
        for arg in args_parts:
            #print "Matching args [%s]" % args
            # An argument with value (=)
            m = re.match("(\w+)=([/\.\-_\w]+)", arg)
            if m:
                self._args_dict[m.group(1)] = m.group(2)
                continue
            m = re.match("(\w+):([/\.\-_\w]+)", arg)
            if m:
                self._args_dict[m.group(1)] = m.group(2)
                continue
            
            # The rest of non value arguments get True
            self._args_dict[arg] = True
        
        self._set_sort_arg()
    
    def _set_sort_arg(self):
        if "sort" in self._args_dict:
            if self._args_dict["sort"] in self._allowed_sorts:
                self._sort_type = self._args_dict["sort"]
            
    def detect_output_type_args(self):
        if "bar" in self._args_dict:
            self._generate_bar_chart = True
        if "pie" in self._args_dict:
            self._generate_pie_chart = True
        if "text" in self._args_dict:
            self._generate_text = True
    
    # This function is extremely slow due to access to object field so it should
    # not be use anymore
    #def update_bucket(self, bucket_name, file_info):
    #    """ Updating a dictionary with relevant counts """
    #    if not bucket_name in self._bucket_dict:
    #        
    #        # A bucket is a dict -- Maybe later we will use an object
    #        self._bucket_dict[bucket_name] = {}
    #        
    #        # Bucket contain its name 
    #        self._bucket_dict[bucket_name]["bucket_name"] = bucket_name 
    #        
    #        #hist_dict[item][item_name] = item # This is done for sorting by the item name
    #        self._bucket_dict[bucket_name]['count'] = 0
    #        self._bucket_dict[bucket_name]['total_size'] = 0
    #        self._bucket_dict[bucket_name]['total_blocks'] = 0
    #    
    #    self._bucket_dict[bucket_name]['count'] += 1
    #    self._bucket_dict[bucket_name]['total_size'] += file_info.st_size
    #    self._bucket_dict[bucket_name]['total_blocks'] += file_info.st_blocks
  
    def register_bucket(self, bucket_name):
        """ An auxilary method to register empty buckets """
         # A bucket is a dict -- Maybe later we will use an object
        self._bucket_dict[bucket_name] = {}
        
        # Bucket contain its name 
        self._bucket_dict[bucket_name]["bucket_name"] = bucket_name 
        
        #hist_dict[item][item_name] = item # This is done for sorting by the item name
        self._bucket_dict[bucket_name]['count'] = 0
        self._bucket_dict[bucket_name]['total_size'] = 0
        self._bucket_dict[bucket_name]['total_blocks'] = 0
    
    # Accessing the file info as a list (obtained from the db) is much much faster
    # (more the 30 times faster) then generating a FileInfo object and accessing its
    # members.
    def update_bucket_from_row(self, bucket_name, row):
        """ Updating a dictionary with relevant counts """
        if not bucket_name in self._bucket_dict:
           self.register_bucket(bucket_name) 
           
        self._bucket_dict[bucket_name]['count'] += 1
        self._bucket_dict[bucket_name]['total_size'] += row[FSFileInfo.FSFileInfo.st_size_index]
        self._bucket_dict[bucket_name]['total_blocks'] += row[FSFileInfo.FSFileInfo.st_blocks_index]
    
    def finalize(self):
        """ Finalize calculations of buckets - such as conersion to MB and totals"""
        # Calculating totals
        self.calc_totals()
       
        for bucket in self._bucket_dict.values():
            
            if bucket["count"] > 0:    
                # Adding the percentage of this bucket from the total
                bucket["count_percent"]        = (float(bucket["count"]) / float(self.totals["count"])) * 100.0
            else:
                bucket["count_percent"] = 0
            
            # Adding the sizes in mb for better readability
            if bucket['total_size'] > 0:
                bucket["total_size_mb"] = bucket['total_size'] / (1024.0 * 1024.0)
                bucket["total_size_mb_percent"]   = (float(bucket["total_size_mb"]) / float(self.totals["total_size_mb"])) * 100
            else:
                bucket["total_size_mb"] = 0
                bucket["total_size_mb_percent"] = 0
                
            if bucket['total_blocks'] > 0:
                bucket["total_blocks_mb"]  = (bucket['total_blocks'] * 512)/ (1024.0 * 1024.0)
                bucket["total_blocks_mb_percent"] = (float(bucket["total_blocks_mb"]) / float(self.totals["total_blocks_mb"])) * 100
            else:
                bucket["total_blocks_mb"]  = 0 
                bucket["total_blocks_mb_percent"] = 0
            
    def calc_totals(self):
        """ Calculate the totals of files size ..."""
        self.total_files = 0
        self.total_mb = 0
        self.total_block_mb = 0
        
        self.totals = {}
        self.totals["count"] = 0
        self.totals["total_size_mb"] = 0
        self.totals["total_blocks_mb"] = 0
        
        for bucket in self._bucket_dict.values():
            self.total_files += bucket['count']
            self.total_mb += bucket['total_size'] / (1024.0 * 1024.0)
            self.total_block_mb += (bucket['total_blocks'] * 512)/ (1024.0 * 1024.0)
            
            self.totals["count"] += bucket['count']
            self.totals["total_size_mb"] += bucket['total_size']
            self.totals["total_blocks_mb"] += bucket['total_blocks']
            
            
            
    def get_sorted_buckets(self):
        sort_by = self._sort_type
        sort_by_field = self._allowed_sorts[sort_by]
        
        #print "Sort By:", sort_by
        sorted_buckets = []
        if sort_by_field == "bucket_name"  and self._bucket_order:
            sorted_buckets = []
            for b in self._bucket_order:
                if b in self._bucket_dict:
                    sorted_buckets.append(self._bucket_dict[b])
        else:
            sorted_buckets = sorted(self._bucket_dict.values(), key=lambda (v): v[sort_by_field])
     
            
     
        for bucket in sorted_buckets:
            bucket["value_sorted_by"] = bucket[sort_by_field]
            
            if(sort_by_field != "bucket_name"):
                bucket["value_sorted_by_percent"] = bucket[sort_by_field + "_percent"]
            else:
                bucket["value_sorted_by_percent"] = 100
                
        return sorted_buckets
    
    def field_total(self, field_name):
        
        bucket_field = self._allowed_sorts[field_name]
        return self.totals[bucket_field]
    
       
    
#import md5
import os.path
import imp
import traceback   
def load_module(code_path):
    try:
        try:
            code_dir = os.path.dirname(code_path)
            code_file = os.path.basename(code_path)

            fin = open(code_path, 'rb')

            return  imp.load_source(code_path, code_path, fin)
        finally:
            try: fin.close()
            except: pass
    except ImportError, x:
        traceback.print_exc(file = sys.stderr)
        raise
    except:
        traceback.print_exc(file = sys.stderr)
        raise
    
    
    
    
    
