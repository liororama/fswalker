# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat

class TimeExecution:
    """ The TimeExecution class is used to time code sections.
        Example:
        time_info = TimeExecution.TimeExecution()
        time_info.enter()
        ... code ...
        time_info.exit()
        time_info.print_time()
    """
    
    def __enter__(self):
        self.start_clock = time.clock()
        self.start_time = time.time()
        return self

    def enter(self):
        return self.__enter__()

    
    def __exit__(self, type, value, traceback):
        self.end = time.clock()
        self.process_time = time.clock() - self.start_clock
        self.wall_time = time.time() - self.start_time
        return True
    
    
    def exit(self):
        self.end = time.clock()
        self.process_time = time.clock() - self.start_clock
        self.wall_time = time.time() - self.start_time
        
    def print_time(self):
        print "Process Time: %.3f" % (self.process_time)
        print "Wall Time:    %.3f" % (self.wall_time)
        
    