#!/usr/bin/env /usr/local/python/2.7.2/bin/python

# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""FSUserInfo object contain all the information about users which were found
   in the file system """

import sys
import os
import os.path
import stat

tbl_fields = [
        {  'name' : "uid",                'type' : "INTEGER",    'prop' : "PRIMARY KEY"  },
        {  'name' : "name",               'type' : "TEXT"        },
        {  'name' : "files",              'type' : "TEXT"        },
        {  'name' : "size_mb",            'type' : "REAL"        },
        {  'name' : "quota_usage_mb",     'type' : "REAL"        },
        {  'name' : "quota_limit_mb",     'type' : "REAL"        },
        {  'name' : "quota_hard_mb",      'type' : "REAL"        },
        {  'name' : "quota_usage_inodes", 'type' : "REAL"        },
        {  'name' : "quota_limit_inodes", 'type' : "REAL"        },
        {  'name' : "quota_hard_inodes",  'type' : "REAL"        },
    ]    
    
def fields_num():
    return len(tbl_fields)

def names_fields_num():
    return len(names_tbl_fields)


class FSUserInfoError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class FSUserInfo:
    """ Contains information retreived about the file from the file system"""

    uid = -1
    name = ""
    files = 0
    size_mb = 0.0
    quota_usage_mb = 0.0
    quota_limit_mb = 0.0
    quota_hard_mb = 0.0
    quota_usage_inodes = 0.0
    quota_limit_inodes = 0.0
    quota_hard_inodes = 0.0
    
    def __init__(self):
        """ Constructor"""
        #self.id = -1
        #self.name = name
        
    def __str__(self):
        return "id: %d name:%s suff:%s mod:%d" %(self.id, self.name, self.suffix, self.st_mode)
        
    def __getattr__(self, name):
        print "Accessing %s" % name
                
    def from_db_row(self, db_row):
        """Setting the file properties from db row"""
    
        ## A sanity check. This is sensitive especially when we still develop the db
        if len(db_row) !=  fields_num():
            err  = "Error: from_db_row error: len of row does not match to db definition"
            err += "Row len %d Expected len %d" % (len(db_row), fields_num())
            raise FSFileInfoError(err)
            
        [ self.uid, self.name, self.files, self.size_mb, self.quota_usage_mb,
          self.quota_limit_mb, self.quota_hard_mb, self.quota_usage_inodes, 
          self.quota_limit_inodes, self.quota_hard_inodes ] = db_row
                

    def to_db_row(self):
        """ Generate a db row in the correct order of fields"""
        #print "Name [%s] [%s] " % (type(self.name), self.name)
        #un = unicode(self.name, "utf-8", errors="ignore")
        
        #name_utf8 = self.name#.encode("utf-8", "ignore")
        #suffix_utf8 = self.suffix#.encode("utf-8", "ignore")

        return [ self.uid, self.name, self.files, self.size_mb, self.quota_usage_mb,
                 self.quota_limit_mb, self.quota_hard_mb, self.quota_usage_inodes, 
                 self.quota_limit_inodes, self.quota_hard_inodes ]
        

if __name__ == "__main__":
 
    ui = FSUserInfo("lior")
    
    u =  ui.bla
    