"""
FSInfoStorage store information about files in a filessystem.
The information save would be th filename, stats() info and possibly\
FS specific info such as lustre stripe unit and stripe count
"""

# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat

import FSFileInfo
import FSWalkInfo
import TimeExecution

import sqlite3 as lite

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from base import Base

class FSInfoStorage:
    """Store the entire file system files info"""
    
    _verbose = False
    _file_tbl = "files_info"
    _users_tbl = "users_info"
    _max_pending_files = 1000
    
    
    def __init__(self, db_file):
        self._db_file = db_file        
        self._files_to_add = []
        self._start_id = 1
    
    def tbl_name(self):
        return self._file_tbl
        
    def set_verbose(self, v):
        self._verbose = v

    def set_db_file(self, db_file):
        self._db_file = db_file
        
    def db_init(self):
        """Initialazing the db creating the files table """
        try:
            if self._verbose : print "Initialazing DB [%s]" % (self._db_file)
            db_file_uri = "sqlite:///" + self._db_file 
            engine = create_engine(db_file_uri, echo = True if self._verbose >= 2 else False)

            Base.metadata.drop_all(engine)
            Base.metadata.create_all(engine)
    
            Session = sessionmaker(bind=engine)
            self._session = Session()
        
        except Exception,e:    
    
            print "Error %s:" % e.args[0]
            sys.exit(1)
        
    
    def db_connect(self):
        """Opening connection to the db"""
        self._db_con = lite.connect(self._db_file)
        #self._db_con.isolation_level = None
        self._db_cur = self._db_con.cursor()    
        db_file_uri = "sqlite:///" + self._db_file
        if self._verbose:
            print "DBFILE:", db_file_uri
        engine = create_engine(db_file_uri, echo = True if self._verbose >= 2 else False)
        Session = sessionmaker(bind=engine)
        self._session = Session()
       
           
    def db_close(self):
        """ Close the DB connection """
        self._db_con.close()
    
    def sql_execute(self, sql_str):
        self._db_cur.execute(sql_str)
        return self._db_cur.fetchall()
        
    def sql_execute_no_fetch(self, sql_str):
        self._db_cur.execute(sql_str)
        
    def fetchone(self):
        return self._db_cur.fetchone()
    
    def fetchmany(self):
        return self._db_cur.fetchmany(size=20000)
    
    def store_walk_info(self, walk_info):
        """ Storing the walk info object in the db """
        try:
            self._session.add(walk_info)
            self._session.commit()
        except Exception, e:
                print "An error occurred:", e.args[0]
                sys.exit(1)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
    
    def get_walk_info(self):
         
        walk_info = self._session.query(FSWalkInfo.FSWalkInfo).first()
        return walk_info
    
    def store_walk_error(self, walk_error):
        """ Storing the walk error object in the db """
        try:
            self._session.add(walk_error)
            self._session.commit()
        except Exception, e:
                print "An error occurred:", e.args[0]
                sys.exit(1)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def get_walk_errors(self):
        """ Getting all the walk errors """
        
        walk_errors = self._session.query(FSWalkInfo.FSWalkErrors).all()
        return walk_errors
    
    
    def add_file(self, file_info):
        """ Add a single file to the list of files to be added to the db"""
        file_info.id = self._start_id
        self._start_id += 1
        self._files_to_add.append(file_info)
        if len(self._files_to_add) >= self._max_pending_files:
            self.store_files()
    
    def store_files(self):
        """Store all files which were not saved yet to the db"""
 
        if self._verbose :
            print "Storing %d files to db" % (len(self._files_to_add))
        
        # No need to store any file
        if len(self._files_to_add) < 1:
            return
        
        time_info = TimeExecution.TimeExecution()
        time_info.enter()
        try:
            self._session.add_all(self._files_to_add)
            self._session.commit()
        except Exception, e:
                print "An error occurred:", e.args[0]
                sys.exit(1)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
           
        time_info.exit()
           
        #print "===================="        
        if self._verbose:
            time_info.print_time()
        
        self._files_to_add = []
        