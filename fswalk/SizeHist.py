# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat
import re
from operator import itemgetter
import abc
import string

import FSFileInfo
import HistBase
import pwd
from sizeof_fmt import sizeof_fmt

class SizeHist(HistBase.HistBase):
    """ Histogram by file size """
    
    def __init__(self):
        """ Constructor """
        self._buckets_upper_sizes = []
        self._buckets_labels      = []
        
        self.add_size_bucket(4096,        "4K")
        self.add_size_bucket(16*1024,     "16K")
        self.add_size_bucket(64*1024,     "64K")
        self.add_size_bucket(256*1024,    "256k")
        self.add_size_bucket(2**20,       "1MB")
        self.add_size_bucket(4*2**20,     "4MB")
        self.add_size_bucket(16*2**20,    "16MB")
        self.add_size_bucket(64*2**20,    "64MB")
        self.add_size_bucket(256*2**20,   "256MB")
        self.add_size_bucket(2**30,       "1GB")
        self.add_size_bucket(4*2**30,     "4GB")
        self.add_size_bucket(16*2**30,    "16GB")
        self.add_last_label(">16GB")
        
        
        self._title = "Size Histogram"    
        self._bucket_name = "size"
        self._bucket_title = "Size"
        self.set_bucket_name_order(self._buckets_labels)
        
    def add_size_bucket(self, size, label):
        """ Adding a bucket definition. The buckets addition should be sorted """
        self._buckets_upper_sizes.append(size)
        self._buckets_labels.append(label)        
        self.register_bucket(label)
        
    def add_last_label(self, label):
        """ Adding the label to indicate all files above last bucket"""
        self._buckets_labels.append(label)
        self.register_bucket(label)
        
    def init(self, args):
        """Initialize the histogram
           args="1k,64k,1M,32M,64M"
        """
        
        return True
    
    #def add_file(self, file_info):
    #    """ Add a file to the histogram """
    #    
    #    # Finding the bucket TODO - optimize this 
    #    
    #    bucket_name = ""
    #    i = 0
    #    for b in self._buckets_upper_sizes:
    #        if file_info.st_size <= b:
    #            break
    #        i +=1
    #    
    #    bucket_name = self._buckets_labels[i]
    #
    #    self.update_bucket(bucket_name, file_info)
    #    return True
    #
    def add_file(self, row):
        """ Add a file to the histogram """
        
        # Finding the bucket TODO - optimize this 
        
        bucket_name = ""
        i = 0
        for b in self._buckets_upper_sizes:
            if row[FSFileInfo.FSFileInfo.st_size_index] <= b:
                break
            i +=1
        
        bucket_name = self._buckets_labels[i]

        self.update_bucket_from_row(bucket_name, row)
        return True
                
    #def to_str(self):
    #    """ Return the histogram as a printable string """
    #    
    #        
    #        hist_str = "%-20s %10s %15s %15s\n" % ("bucket(KB)", "count", "size(MB)", "blocks(MB)")
    #    
    #        for b in self._buckets_info:
    #            count = b['count']
    #            file_size = b['size_mb'] / (1024.0 * 1024.0)
    #            block_size = (b['blocks_mb'] * 512) / (1024.0 * 1024.0)
    #            
    #            hist_str += "%-20d %10d %15.1f %15.1f\n" % (b['bucket']/1024, count, file_size, block_size)
    #        
    #        
    #        
    #    return hist_str
          
