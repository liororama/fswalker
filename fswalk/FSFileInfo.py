"""FSFileInfo object contain all the information about the file retreived from
the filesystem
"""
# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import os.path
import stat

import sqlalchemy

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Sequence
from sqlalchemy import Column, Integer, Float, String

from base import Base

tbl_fields = [
        {  'name' : "id",        'type' : "INTEGER",    'prop' : "PRIMARY KEY"  },
        {  'name' : "name",      'type' : "TEXT"        },
        {  'name' : "suffix",    'type' : "TEXT"        },
        {  'name' : "file_type", 'type' : "TEXT"        },
        {  'name' : "st_mode",   'type' : "INTEGER"     },
        {  'name' : "st_inode",  'type' : "INTEGER"     },
        {  'name' : "st_dev",    'type' : "INTEGER"     },
        {  'name' : "st_nlinks", 'type' : "INTEGER"     },
        {  'name' : "st_uid",    'type' : "INTEGER"     },
        {  'name' : "st_gid",    'type' : "INTEGER"     },
        {  'name' : "st_size",   'type' : "INTEGER"     },
        {  'name' : "st_atime",  'type' : "REAL"  },
        {  'name' : "st_mtime",  'type' : "REAL"  },
        {  'name' : "st_ctime",  'type' : "REAL"  },
        {  'name' : "st_blocks", 'type' : "INTEGER"},
    ]

names_tbl_fields = [
        {  'name' : "id",        'type' : "INTEGER",    'prop' : "PRIMARY KEY"  },
        {  'name' : "name",      'type' : "TEXT"        },
]

    
def fields_num():
    return len(tbl_fields)

def names_fields_num():
    return len(names_tbl_fields)


class FSFileInfoError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def get_index(index):
    val = index["i"]
    index["i"] += 1
    return val

class FSFileInfo(Base):
    """ Contains information retreived about the file from the file system"""

    __tablename__ = 'files_info'
    id           = Column(Integer, Sequence('file_id_seq'), primary_key=True)
    name         = Column(String)
    suffix       = Column(String)
    file_type    = Column(String)
    
    st_mode      = Column(Integer)
    st_inode     = Column(Integer)
    st_dev       = Column(Integer)
    st_nlinks    = Column(Integer)
    st_uid       = Column(Integer)
    st_gid       = Column(Integer)
    st_size      = Column(Integer)
    st_atime     = Column(Float)
    st_mtime     = Column(Float)
    st_ctime     = Column(Float)
    st_blocks    = Column(Integer)

    index = { "i" : 0}
    id_index = get_index(index)
    name_index = get_index(index)
    suffix_index = get_index(index)
    file_type_index = get_index(index)
    st_mode_index = get_index(index)
    st_inode_index = get_index(index)    
    st_dev_index = get_index(index)      
    st_nlinks_index = get_index(index)   
    st_uid_index = get_index(index)      
    st_gid_index = get_index(index)      
    st_size_index = get_index(index)     
    st_atime_index = get_index(index)    
    st_mtime_index = get_index(index)    
    st_ctime_index = get_index(index)    
    st_blocks_index = get_index(index)  

            
    def __init__(self, name):
        
        self.id = -1
        self.name = name
        self.suffix = os.path.splitext(name)[1][1:].strip()
        self.file_type = ""
        
    def __str__(self):
        return "id: %d name:%s suff:%s mod:%d" %(self.id, self.name, self.suffix, self.st_mode)

    def __repr__(self):
        return "<File('%s','%s', '%d')>" % (self.name, self.file_type, self.st_mode)
     
    def set_stat_info(self, file_stat):
        [ self.st_mode, self.st_inode, self.st_dev, self.st_nlinks,
          self.st_uid, self.st_gid, self.st_size, self.st_atime,
          self.st_mtime, self.st_ctime] = file_stat

        self.st_blocks = file_stat.st_blocks

        if stat.S_ISREG(self.st_mode) :
            self.file_type = "file"
        elif stat.S_ISDIR(self.st_mode) :
            self.file_type = "dir"
        elif stat.S_ISLNK(self.st_mode) :
            self.file_type = "link"
        else:
            self.file_type = "other"


    def from_db_row(self, db_row):
        """Setting the file properties from db row"""
    
        # A sanity check. This is sensitive especially when we still develop the db
        if len(db_row) !=  fields_num():
            err  = "Error: from_db_row error: len of row does not match to db definition"
            err += "Row len %d Expected len %d" % (len(db_row), fields_num())
            raise FSFileInfoError(err)
            
        [self.id, self.name, self.suffix, self.file_type,
         self.st_mode, self.st_inode, self.st_dev, self.st_nlinks,
         self.st_uid, self.st_gid, self.st_size, self.st_atime,
         self.st_mtime, self.st_ctime,
         self.st_blocks] = db_row
                

    def to_db_row(self):
        """ Generate a db row in the correct order of fields"""
        #print "Name [%s] [%s] " % (type(self.name), self.name)
        #un = unicode(self.name, "utf-8", errors="ignore")
        
        name_utf8 = self.name#.encode("utf-8", "ignore")
        suffix_utf8 = self.suffix#.encode("utf-8", "ignore")
        return [self.id, name_utf8, suffix_utf8, self.file_type, self.st_mode, self.st_inode, self.st_dev, self.st_nlinks,
         self.st_uid, self.st_gid, self.st_size, self.st_atime, self.st_mtime, self.st_ctime,
         self.st_blocks]
        

