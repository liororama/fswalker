# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
ColorLogger module for adding color support to python logging module

Author: Lior Amar

Usage example:
=============================
logging.setLoggerClass(ColorLogger.ColorLogger)  
logger = logging.getLogger("my_module")
logger.setLevel(logging.DEBUG)

# For example we connect the collor formatter to a tty 
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

color_formatter = ColorLogger.ColorFormatter(ColorLogger.simple_format_str)
ch.setFormatter(color_formatter)
logger.addHandler(ch)

logger.debug_r("DDDDDDD_RRRRRRRR")
logger.debug_b("BBBBBBB BBBBBBBB")
=============================

"""

import re
import logging

# Default format strings for convenience
default_format_str = ("%(asctime)s - [%(name)-20s][%(levelname)-18s]  %(message)s  (%(filename)s:%(lineno)d)")
simple_format_str = ("[%(name)-15s][%(levelname)-10s] %(message)s ")


class ColorFormatter(logging.Formatter):
    """ Color Formatter class : injecting color into the message
    The class should given as an argument to the setFormatter method of the handler
    """

    # Some class variables 
    format_str = ""
    
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

    RESET_SEQ = "\033[0m"
    COLOR_SEQ = "\033[1;%dm"
    BOLD_SEQ = "\033[1m"

    # Translation from debug levels to color
    COLORS = {
        'DEBUG'        : GREEN,
        'WARNING'      : YELLOW,
        'INFO'         : WHITE,
        'CRITICAL'     : RED,
        'ERROR'        : RED,
        'RED'          : RED,
        'GREEN'        : GREEN,
        'YELLOW'       : YELLOW,
        'BLUE'         : BLUE,
        'MAGENTA'      : MAGENTA,
        'CYAN'         : CYAN,
        'WHITE'        : WHITE,
        'DEBUG_R'      : RED,
        'DEBUG_G'      : GREEN,
        'DEBUG_B'      : BLUE,
        'DEBUG_Y'      : YELLOW,
        'DEBUG_C'      : CYAN,
        'DEBUG_M'      : MAGENTA
    }

    #def formatter_msg(self, msg, use_color = True):
    #    """ Formatting the basic message """
    # if use_color = False we return the message as is
        #if not use_color:
         #   return msg
    
    # Replacing the parts with colors
    
    #msg = re.sub("\(asctime\)", )
    
    #msg.replace("$RESET", self.RESET_SEQ).replace("$BOLD", self.BOLD_SEQ)
    #else:
    #  msg = msg.replace("$RESET", "").replace("$BOLD", "")
    #    return msg

    def __init__(self, format_str = None, use_color=True):
        """ Modifying the format string to contain color definitions """
        self.use_color = use_color
        self._color_date = False
        self._color_level = True
        self._color_name = True
        self._color_message = True
        self._color_filename = True
        
        
        # Taking the default format
        if not format_str:
            format_str = default_format_str
            
        #format_str = self.formatter_msg(format_str, use_color)
        
        # Setting the format to the formatter parent class
        self.format_str = format_str
        logging.Formatter.__init__(self, format_str)

    @property
    def color_date(self):
        """ Getter """
        return self._color_date
    @color_date.setter
    def color_date(self, v):
        """ Setter """
        self._color_date = v
        
    @property
    def color_level(self):
        return self._color_level
    @color_level.setter
    def color_level(self, v):
        self._color_level = v
    
    @property
    def color_name(self):
        return self._color_name
    @color_name.setter
    def color_level(self, v):
        self._color_name = v
    
    @property
    def color_message(self):
        return self._color_message
    @color_message.setter
    def color_message(self, v):
        self._color_message = v
    
    @property
    def color_filename(self):
        return self._color_filename
    @color_filename.setter
    def color_filename(self, v):
        self._color_filename = v
    
    def fix_record_levelname(self, record):
        record.levelname = re.sub("DEBUG_[A-Z]*", "DEBUG", record.levelname)
        
    def format(self, record):
        """ Formatting the message """

        # Setting back the level name to be debug without the _color
        orig_levelname = record.levelname
        self.fix_record_levelname(record)
        
        # Modifing the record parts to contain color sequence
        if self.use_color and record.levelname in self.COLORS:
            fore_color = 30 + self.COLORS[orig_levelname]
           
            if self.color_date:
                record.asctime = self.COLOR_SEQ % fore_color + record.asctime + self.RESET_SEQ
            
            if self.color_level:    
                record.levelname = self.COLOR_SEQ % fore_color + record.levelname + self.RESET_SEQ
           
            if self.color_name:    
                record.name = self.COLOR_SEQ % fore_color + record.name + self.RESET_SEQ
            
            if self.color_message:
                record.msg =  self.COLOR_SEQ % fore_color + record.msg + self.RESET_SEQ
           
            if self.color_filename:
                record.filename =  self.COLOR_SEQ % fore_color + record.filename + self.RESET_SEQ
           
            #if self.color_lineno:
            #    record.lineno =  self.COLOR_SEQ % fore_color + record.lineno + self.RESET_SEQ
        
        
        # Asking the formatter class to do the rest of the work
        msg =  logging.Formatter.format(self, record)
        return msg
    
class ColorLogger(logging.getLoggerClass()):
    """
    A logger with additional debug methods with colors (such as debug_r)
    To use this calss you first should call
      logging.setLoggerClass(ColorLogger.ColorLogger)  
    and only then get a logger
      logger = logging.getLogger("my_module")
    
    """
    
    # Setting extra debug levels with different color for each level    
    DEBUG_RED     = logging.DEBUG + 1
    DEBUG_BLUE    = logging.DEBUG + 2
    DEBUG_GREEN   = logging.DEBUG + 3
    DEBUG_YELLOW  = logging.DEBUG + 4
    DEBUG_CYAN    = logging.DEBUG + 5
    DEBUG_MAGENTA = logging.DEBUG + 6
    
    def __init__(self, name):
        logging.Logger.__init__(self, name)                
        # Adding the new DEBUG_XX levels
        logging.addLevelName(self.DEBUG_RED, "DEBUG_R")
        logging.addLevelName(self.DEBUG_BLUE, "DEBUG_B")
        logging.addLevelName(self.DEBUG_GREEN, "DEBUG_G")
        logging.addLevelName(self.DEBUG_YELLOW, "DEBUG_Y")
        logging.addLevelName(self.DEBUG_CYAN, "DEBUG_C")
        logging.addLevelName(self.DEBUG_MAGENTA, "DEBUG_M")
        
    # Net debug_XX methods
    def debug_r(self, msg, *args, **kw):
        self.log(self.DEBUG_RED, msg, *args, **kw)
    
    def debug_b(self, msg, *args, **kw):
        self.log(self.DEBUG_BLUE, msg, *args, **kw)

    def debug_g(self, msg, *args, **kw):
        self.log(self.DEBUG_GREEN, msg, *args, **kw)

    def debug_y(self, msg, *args, **kw):
        self.log(self.DEBUG_YELLOW, msg, *args, **kw)

    def debug_c(self, msg, *args, **kw):
        self.log(self.DEBUG_CYAN, msg, *args, **kw)

    def debug_m(self, msg, *args, **kw):
        self.log(self.DEBUG_MAGENTA, msg, *args, **kw)
