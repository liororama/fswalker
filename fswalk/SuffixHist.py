# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat
import re
from operator import itemgetter
import abc
import string

import FSFileInfo
import HistBase

class SuffixHist(HistBase.HistBase):
    """ Histogram by file suffix """
    #_allowed_sorts = { "name" : "bucket_name",
    #                   "size" : "total_size",
    #                   "files": "count"}
    #
    def __init__(self):
        """ Constructor """
        self._bucket_dict = {}    
        self._print_header = False
        self._title = "Suffix Histogram"    
        self._bucket_name = "suffix"
        self._bucket_title = "Suffix"
        
    def _parse_args(self, args):    
        if not args:
            return
    
        self._prepare_args(args, ",")
        
        if "sort" in self._args_dict:
            sort_type = self._args_dict["sort"]
            if not sort_type in self._allowed_sorts:
                self._set_err("Error: sort type %s is not supported" % sort_type)
                return False
            self._sort_type = sort_type
        
        if "header" in self._args_dict:
            self._print_header = True
        if "total" in self._args_dict:
            self._print_total = True


    def init(self, args):
        """Initialize the histogram"""
        
        #print "args: [%s]" % args
        # Parsing arguments
        
        self._parse_args(args)        
        return True
    
    def add_file(self, file_info):
        """ Add a file to the histogram. In this class the suffix is used as the bucket name"""
        #print "Adding file %s" % (file_info.name)
        bucket_name = file_info.suffix
        if not bucket_name:
            bucket_name = "no-suffix"
        self.update_bucket(bucket_name, file_info)
        return True
    
    def save_to_file(self, file_name):
        """ Save the histogram to a file """
