"""
DUHist - Implementing the du of subdirectories via the histogram mechanism
"""
# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import HistBase
from FSFileInfo import FSFileInfo

class DUHist(HistBase.HistBase):
    """ Histogram of to directories (under a given dir) """
    
        
    
    def __init__(self):
        """ Constructor """
    #    self._bucket_dict = {}    
        self._title = "DU"    
        self._bucket_name = "Name"
        self._bucket_title = "Name"
    
        self._base_dir_depth = 0 
    
    @property
    def base_dir_depth(self):
        """ Getter """
        return self._base_dir_depth
    
    @base_dir_depth.setter
    def base_dir_depth(self, v):
        self._base_dir_depth = v
        
    
    def init(self, args):
        """Initialize the histogram"""
        
        #print "args: [%s]" % args
        # Parsing arguments
        
        #return self._parse_args(args)        
        self._prepare_args(args)
        return True        

    def add_file(self, row):
        """ Add a file to the histogram """
        
        path_name = row[FSFileInfo.name_index]
        path_name2 = path_name.strip("/")
        parts = path_name2.split('/')

        # Checking if current file is under a directory in base
        parts_num = len(parts)
    
        #print "File [{0}]".format(path_name2)
        
        if parts_num > self._base_dir_depth + 1:
            sub_dir = parts[self._base_dir_depth]
            self.update_bucket_from_row(sub_dir, row)

        return True
    
    def to_str(self):
        """ Return the histogram as a printable string """
      
        hist_str = ""
        try:
            # This will create a new file or **overwrite an existing file**.
            if self._print_header:
                hist_str = "%-20s %10s %15s %15s\n" % ("directory", "count", "size(MB)", "blocks(MB)")

            sort_by
            if self._sort_type == "name":
                #sorted_suff_items = sorted(self._bucket_dict.values(), key=lambda (v): v["name"])
                sorted_suff_items = self.get_sorted_buckets("name")
            elif self._sort_type == "size":
                sorted_suff_items = sorted(self._bucket_dict.values(), key=lambda (v): v["total_size"])
            elif self._sort_type == "files":
                sorted_suff_items = sorted(self._bucket_dict.values(), key=lambda (v): v["count"])

            #print sorted_suff_items
            total_files = 0
            total_mb = 0
            total_blocks_mb = 0
            for dir_item in sorted_suff_items:
            
                dir_count = dir_item['count']
                dir_size_mb = dir_item['total_size'] / (1024.0 * 1024.0)
                dir_blocks_mb = (dir_item['total_blocks'] * 512)/ (1024.0 * 1024.0)
                
                total_files += dir_count
                total_mb += dir_size_mb
                total_blocks_mb += dir_blocks_mb
        
                hist_str += "%-20s %10d %15.1f %15.1f\n" % (dir_item["name"], dir_count, dir_size_mb, dir_blocks_mb)
                f.write("%s %d %f %f\n" % (dir_item["name"], dir_count, dir_size_mb, dir_blocks_mb)) # Write a string to a file
            f.close()
        
        except IOError:
            print "IO Error"
            pass
        
        # Printing the totals
        if self._print_totals:
            hist_str += "\n"
            hist_str += "%-20s %10d %15.1f %15.1f\n" % ("Total", total_files, total_mb, total_blocks_mb)
            
        
        return hist_str
        
        
