#!/usr/bin/env python

import logging
import ColorLogger

#create logger with "spam_application"
logging.setLoggerClass(ColorLogger.ColorLogger)  
logger = logging.getLogger("tester")
logger.setLevel(logging.DEBUG)

#create file handler and set level to debug
fh = logging.FileHandler("/tmp/colorlog_tester.log")
fh.setLevel(logging.DEBUG)

#create console handler and set level to error
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)


#create formatter
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s -  %(message)s")

fh.setFormatter(formatter)

#ch.setFormatter(formatter)
color_formatter = ColorLogger.ColorFormatter(ColorLogger.simple_format_str)
color_formatter.color_message = True
color_formatter.color_level = True
color_formatter.color_date = True

ch.setFormatter(color_formatter)
#logger.addHandler(handler)


#add fh to logger
logger.addHandler(fh)
#add ch to logger
logger.addHandler(ch)

logger.info("creating an instance of auxiliary_module.Auxiliary")
logger.debug("debug message")

print 
logger.debug_r("DDDDDDD_RRRRRRRR")
logger.debug_b("BBBBBBB BBBBBBBB")
logger.debug_g("GGGGGGGGGGGGGGGG")
logger.debug_y("YYYYYYYYYYYYYYYY")
logger.debug_c("CCCCCCCCCCCCCCCC")
logger.debug_m("MMMMMMMMMMMMMMMM")

logger.setLevel(logging.ERROR)
print
print "No printouts should be seen"
logger.debug_c("CCCCCCCCCCCCCCCC")
logger.debug_m("MMMMMMMMMMMMMMMM")

print 
print 
print "Going back to debug"
print
print
logger.setLevel(logging.DEBUG)
logger.warn("warning message")
logger.error("error message")
logger.fatal("fatal message")
logger.info("created an instance of auxiliary_module.Auxiliary")
logger.info("calling auxiliary_module.Auxiliary.do_something")


