
def sizeof_fmt(num):
    """ Convert a number of bytes to a human readable size """
    for x in ["B", 'KB', 'MB', 'GB']:
        if num < 1024.0 and num > -1024.0:
            return "%3.1f%s" % (num, x)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')
    
    
def sizeof_fmt_from_mb(num):
    """ Convert a number of megabytes to a human readable size """
    for x in ['MB', 'GB']:
        if num < 1024.0 and num > -1024.0:
            return "%3.1f%s" % (num, x)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')
    
    