# Copyright (c) 2012, Lior Amar (liororama at gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without 
# modification, are not permitted without a written permission from
# Lior Amar (the copyright holder)
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

"""FSDBInfo object cotain general information about the db
The object is used to store and retrieve the information from the database.
The db contain only one such object (table with one line).
"""

import sys
import os
import os.path
import stat
import errno

import sqlalchemy

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Sequence
from sqlalchemy import Column, Integer, Float, String
from sqlalchemy import types

from base import Base

import datetime

class FSDBInfoError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def now():
    return datetime.datetime.now()

class FSWalkInfo(Base):
    """ Contains information retreived about the file from the file system"""

    __tablename__ = 'walk_info'
    directory     = Column(String, primary_key=True)
    total_size_mb = Column(Float)
    total_files   = Column(Integer)
    total_dirs    = Column(Integer)
    max_depth     = Column(Integer)
    start_timestamp = Column(Integer)
    start_date    = Column(types.DateTime)            
    duration_sec  = Column(Float)
    total_errors  = Column(Integer)
    
    def __init__(self):
        self.total_size_mb = 0.0
        self.total_errors = 0        
        
    def __str__(self):
        s  = "Directory:        {0}\n".format(self.directory)
        s += "Total Size (MB):  {0:.2f}\n".format(self.total_size_mb) 
        s += "Total Files:      {0}\n".format(self.total_files) 
        s += "Total Dirs:       {0}\n".format(self.total_dirs) 
        s += "Max Depth:        {0}\n".format(self.max_depth)
        s += "Start Timestamp:  {0}\n".format(self.start_timestamp)
        s += "Start Date:       {0}\n".format(self.start_date)
        s += "Duration (sec):   {0:.2f}\n".format(self.duration_sec)
        s += "Total Errors:     {0}\n".format(self.total_errors)
        
        return s       
    #def __repr__(self):
    #    return "<File('%s','%s', '%d')>" % (self.name, self.file_type, self.st_mode)
    #
    
class FSWalkErrors(Base):
    """ Contains information retreived about the file from the file system"""

    __tablename__ = 'walk_errors'
    file_path     = Column(String, primary_key=True)
    errno         = Column(Integer)
    
    def __init__(self):
        """ No special thing to do """        
        
    def __str__(self):
        s  = "{0:30s}   {1:s}".format(self.file_path, os.strerror(self.errno))
        return s       
    