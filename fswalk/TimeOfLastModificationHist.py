# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat
import re
from operator import itemgetter
import abc
import string

import FSFileInfo
import HistBase
import pwd        
   
class TimeOfLastModificationHist(HistBase.HistBase):
    """ Histogram by file time """
            # time_t    st_atime;   /* time of last access */
            ## -> time_t    st_mtime;   /* time of last modification */
            # time_t    st_ctime;   /* time of last status change */
    
    sec_in_day = 60*60*24
    
    def __init__(self, curr_time=None):
        """ Constructor """
        if not curr_time:
            self.curr_time = int(time.time())
        else:
            self.curr_time = curr_time
            
        print "Current Unix time (before): ", self.curr_time
        # one year period of time
        self._buckets_era=[]
        self._buckets_labels= []

        self.register_bucket_era(self.sec_in_day * 1,   "1 Day")
        self.register_bucket_era(self.sec_in_day * 3,   "3 Days")
        self.register_bucket_era(self.sec_in_day * 7,   "7 Days")
        self.register_bucket_era(self.sec_in_day * 14,  "14 Days")
        self.register_bucket_era(self.sec_in_day * 30,  "1 Month")
        self.register_bucket_era(self.sec_in_day * 191, "3 Month")
        self.register_bucket_era(self.sec_in_day * 183, "6 Month")
        self.register_bucket_era(self.sec_in_day * 365, "12 Month")
        self.register_last_label(">12 Month")
        
        
        self._title = "Modification Time Histogram"    
        self._bucket_name = "time"
        self._bucket_title = "Time"
        self.set_bucket_name_order(self._buckets_labels)

        
    def register_bucket_era(self, seconds, label):
        """ Adding a bucket to track: seconds is the number of seconds since the walk
            to measure. For example 60*60*24 will track all files which are less(equal)
            then 1 day of change and more then the next bucket
        """
        self._buckets_era.append(self.curr_time - seconds)
        self._buckets_labels.append(label)
        self.register_bucket(label)
        
    def register_last_label(self, label):
        self._buckets_labels.append(label)    
        self.register_bucket(label)
        
    def init(self, args):
        """Initialize the histogram
        """
        return True
                
        
    def add_file(self, fi_row):
        """ Add a file to the histogram according to the modification time"""
        i = 0
        for b in self._buckets_era:
            if fi_row[FSFileInfo.FSFileInfo.st_mtime_index] >= b :
                break
            i +=1
        
        bucket_name = self._buckets_labels[i]        
        self.update_bucket_from_row(bucket_name, fi_row)
        
        return True                
    