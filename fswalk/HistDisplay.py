# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat
import re
from operator import itemgetter
import abc
import string

import FSFileInfo
from sizeof_fmt import sizeof_fmt_from_mb

import HistBase

def get_bucket_line(bucket, human=False):
    l = ""
   
    
    if human :
        l = "%-20s %10d (%5.2f) %15s (%5.2f) %15s \n" % \
            (bucket["bucket_name"],
             bucket["count"],
             bucket["count_percent"],
             sizeof_fmt_from_mb(bucket["total_size_mb"]),
             bucket["total_size_mb_percent"],
             sizeof_fmt_from_mb(bucket["total_blocks_mb"]))
    else:
        l = "%-20s %10d (%5.2f) %15.1f (%5.2f) %15.1f  %10.2f\n" % \
            (bucket["bucket_name"],
             bucket["count"],
             bucket["count_percent"],
             bucket["total_size_mb"],
             bucket["total_size_mb_percent"],
             bucket["total_blocks_mb"],
             bucket["value_sorted_by_percent"])
    return l

def hist_to_text(hist,
                 header=False,
                 total=False,
                 min_percent=0,
                 human = False):
    """ Return the histogram as a printable string """
  
    hist_str = ""
    
    if header:
        hist_str += "%-20s %10s (%5s) %15s (%5s) %15s\n" % (hist.bucket_title, "count", "%", "size(MB)", "%", "blocks(MB)")
    
    sorted_buckets = hist.get_sorted_buckets()
    
    other = {
        "bucket_name"              : "Other",
        "count"                     : 0,
        "total_size_mb"             : 0,
        "total_blocks_mb"           : 0,
        "value_sorted_by_percent"   : 0
    }
    
    for bucket in sorted_buckets:
        if min_percent == 0 or ((float(min_percent) > 0.0) and (float(bucket["value_sorted_by_percent"]) > float(min_percent))):
            hist_str += get_bucket_line(bucket, human)
        else:
            other["count"] += bucket["count"]
            other["total_size_mb"] += bucket["total_size_mb"]
            other["total_blocks_mb"] += bucket["total_blocks_mb"]
            other["value_sorted_by_percent"] += bucket["value_sorted_by_percent"]
            
    if min_percent > 0:
            hist_str += get_bucket_line(other, human)

    # Adding the totals
    if total:
        hist.calc_totals()
        if human:
            hist_str += "\n%-20s %10d %15s %15s %10s\n" % ("Total", hist.total_files,
                                                           sizeof_fmt_from_mb(hist.total_mb),
                                                           sizeof_fmt_from_mb(hist.total_block_mb),
                                                           " ")
        else:
            hist_str += "\n%-20s %10d %15.1f %15.1f %10s\n" % ("Total", hist.total_files, hist.total_mb, hist.total_block_mb, " ")
    return hist_str



def get_bar_data(hist, value_name, min_percent):
    """ Return a list of the sorted histogram ready to be ploted as bar """
    
    sorted_buckets = hist.get_sorted_buckets()
    
    value_total = hist.field_total(value_name)
    x = []
    labels = []
    other_value = 0
    other_bucket_count = 0
    
    for b in sorted_buckets:
        b_value = b[hist._allowed_sorts[value_name]] 
        bucket_percent = (float(b_value) / float(value_total))*100.0
        #print "B percent : Value: {0} Total {1} Percent: {2:.3f} ".format(b_value, value_total, bucket_percent)
        if bucket_percent > min_percent:
            x.append(b_value)
            labels.append(b["bucket_name"])
        else:
            other_bucket_count += 1
            other_value += b_value
    
    # adding the sum of the small buckets (which are less then min percent) at the end    
    if other_bucket_count > 0:
        x.append(other_value)
        labels.append("Other")
    
    return (x, labels)

def hist_to_bar_chart(hist,
                      file_name,
                      value_name = "files",
                      min_percent = 1,
                      min_percent_label = "Other"):
    """ Generating a bar chart from the histogram"""
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.artist as artist
    from matplotlib.pyplot import figure, show, cm
  
    (x, labels) = get_bar_data(hist, value_name, min_percent)
    print x
    print labels
    ind = np.arange(len(x))
    
    fig = figure()
    print artist.getp(fig.patch)
    plt.set_background
    #xmin, xmax = xlim = 0,10
    #ymin, ymax = ylim = 0,10
    #ax = fig.add_subplot(111, xlim=xlim, ylim=ylim,
    #                    autoscale_on=False)
    #X = [[.8, .8],[.7,.7]]

    #ax.imshow(X, interpolation='bicubic', cmap=cm.summer,
    #         extent=(xmin, xmax, ymin, ymax),
    #         alpha=1)

    b1 = plt.bar(ind, x)
    plt.xticks(np.arange(len(x))+0.5,labels, rotation="vertical")
    plt.ylabel(hist._sort_type)
    plt.title(hist._title + " (sorted by " + hist._sort_type + ")")
    

    plt.savefig("/tmp/bb.png")
    plt.show()
    
    
    
def hist_to_pie_chart(hist):
    """ Creating a pie chart from the histogram """
    
    
    