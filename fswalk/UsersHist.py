# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat
import re
from operator import itemgetter
import abc
import string

import FSFileInfo
import HistBase
import pwd  

class UsersHist(HistBase.HistBase):
    """ Histogram by users/uids """
    
    def __init__(self):
        """ Constructor """
        self._bucket_dict = {}
        
        self._title = "Users Histogram"    
        self._bucket_name = "user"
        self._bucket_title = "User Name"
   
        self._uid_cache = { 0 : "root"}
    
    def init(self, args):
        """Initialize the histogram"""
        return True
    
    def add_file(self, file_info):
        """ Add a file to the histogram """
        
        #print "Adding file %s" % (file_info.name)
        
        uid = file_info.st_uid
        user = ""
        
        if uid in self._uid_cache:
            user = self._uid_cache[uid]
        else:
            user = self.get_user(uid)
            self._uid_cache[uid] = uid
        self.update_bucket(user, file_info)
        
        return True

    def get_user(self, uid):
        """ Get username from uid, if fail return uid"""    
        try:
            p = pwd.getpwuid(uid)
            return p.pw_name
        except:
            return uid
                
        
        
    def save_to_file(self, file_name):
        """ Save the histogram to a file """
