#!/usr/bin/env /usr/local/python/2.7.2/bin/python

# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import stat

class DaemonPIDFile:
    """Managing the daemon lock file at /var/run"""
    
    _verbose = False
    _run_dir = "/var/run"
    _error = ""
    
    def __init__(self, file_name):
        self._pid_file = file_name
    
    def set_file(self, file_name):
        self._pid_file = file_name
    
    def set_verbose(self, v):
        self._verbose = v
        
    def get_pid_from_file(self, pid_list2):
        if os.path.isfile(self._pid_file):
            fh = open(self._pid_file, "r")
            pid_str = fh.readline()
            pid = int(pid_str)
            if self._verbose:
                print "Got pid [%d]" % (pid)
            del pid_list2[:]
            pid_list2.append(pid)
            return True
        else:
            self._error = "No file [%s]" % self._pid_file 
            return False
    
    def start(self, pid):
        """ Starting the daemon using pid file """
        if os.path.isfile(self._pid_file):
            return False
        else:
            f = open(self._pid_file, "w")
            f.write("%s" %(pid))
            f.close
    
        return True
    
    def stop(self, pid):
        """ Stopping the daemon removing the file"""
        
        if os.path.isfile(self._pid_file):
            pid_list = []
            res = self.get_pid_from_file(pid_list)
            if res == False:
                self._error = "Stopping: no such file [%s]" % self._pid_file
                return False
            if pid_list[0] != pid:
                self._error = "Stopping: pid in file [%s] is not [%d]" % (self._pid_file, pid)
                return False
            os.unlink(self._pid_file)
            return True
                
        
    def force_start(self,pid):
        """ Start even if you already have a pid file
            first try to see if the pid is alive if not then remove
            and generate a new file """
        if not os.path.isfile(self._pid_file):
            return self.start(pid)
        
        pid_list = []
        res = self.get_pid_from_file(pid_list)
        if res == False:
            self._error = "Force-start: no such file [%s]" % self._pid_file
            return False
        if pid_list[0] != pid:
            try:
                os.kill(pid_list[0], 0)
                self._error = "Force-start: other process [%d] is still running"
                return False
            except:
                os.unlink(self._pid_file)
                return self.start(pid)
        else:
            # Pids are equal
            self._error = "Force-start: pid [%d] in file is equal to provided pid"
            return False
        self._error = "Force-start: hmm bummer should not arrive to this line"
        return False
    
#if __name__ == "__main__":
#    d = DaemonPIDFile("/tmp/ddd.pid")
#    pid_list = []
#    res = d.get_pid_from_file(pid_list)
#    if res == True:
#        print pid_list
#        print "Found pid file [%d]" % (pid_list[0])
#
#    d.start(1111)
#    d.stop(2222)
#    print d._error
#    d.stop(1111)
#    d.start(2222)
#    d.force_start(3333)