#!/usr/bin/env /usr/local/python/2.7.2/bin/python

import os
import sys
import time
import sqlalchemy

import TimeExecution

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Sequence

Base = declarative_base()

from sqlalchemy import Column, Integer, String
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)
    password = Column(String)

    def __init__(self, name, fullname, password):
        self.name = name
        self.fullname = fullname
        self.password = password
    
    def __repr__(self):
        return "<User('%s','%s', '%s')>" % (self.name, self.fullname, self.password)
        
class FileInfo(Base):
    __tablename__ = 'files_info'
    id         = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    name       = Column(String)
    suffix     = Column(String)
    file_type  = Column(String)
    st_mod     = Column(Integer)
    
    def __init__(self, name):
        self.name = name
        self.st_mode = 5555
        self.suffix = "dddd" #os.path.splitext(name)[1][1:].strip()
        self.file_type = "directory"

    

    def __repr__(self):
        return "<File('%s','%s', '%d')>" % (self.name, self.file_type, self.st_mode)
        

def generate_files():
    file_list = []
    
    for i in range(1, 10000):
        p = "/fffff-%d" % (i)
        fi = FileInfo(p)
        #print fi
        file_list.append(fi)
    return file_list

def main():
    print User.__table__
    print FileInfo.__table__
    
    engine = create_engine('sqlite:////tmp/sqla.db', echo=False)

    Base.metadata.create_all(engine)
    ed_user = User('ed', 'Ed Jones', 'edspassword')

    Session = sessionmaker(bind=engine)
    session = Session()
#    session.add(ed_user)
 
    fl = generate_files()
    print "Generated file objects"
    time_info = TimeExecution.TimeExecution()
    time_info.enter()
    #time.sleep(1)
    try:
        print "In try"
        #print "Adding:", fi
        session.add_all(fl)
    
        #session.dirty
        #session.new
        session.commit()

    except Exception,e:
        print "Got exception", e
        
    time_info.exit()
    time_info.print_time()
    
    return 0

if __name__ == "__main__":
    res = main()
    sys.exit(res)
    


