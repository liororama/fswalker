# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Implementation of the prefix Histogram """

import sys
import os
import time
import stat
import re
from operator import itemgetter
import abc
import string

import FSFileInfo
import HistBase

class PrefixHist(HistBase.HistBase):
    """ Histogram by file prefixes """
    
    _sort_type = "name"
    _allowed_sorts = { "name" : 1,
                       "size" : 1,
                       "files": 1}
    
    _print_header = False
    _print_totals = False
    _args_dict = {}
    _prefix_method = "by_sep"
    _prefix_sep = "."
    
    def __init__(self):
        """ Constructor """
        self._prefix_dict = {}    
            
        
    def _parse_args(self, args):    
        if not args :
            return
    
        self._prepare_args(args)
        
        if "sort" in self._args_dict:
            sort_type = self._args_dict["sort"]
            if not sort_type in self._allowed_sorts:
                self._set_err("Error: sort type %s is not supported" % sort_type)
                return False
            self._sort_type = sort_type
        
        if "header" in self._args_dict:
            self._print_header = True
        if "totals" in self._args_dict:
            self._print_totals = True

        if "sep" in self._args_dict:
            self._prefix_method = "by_sep"
            self._prefix_sep = self._args_dict["sep"]
        if "python" in self._args_dict:
            print "Got python args"
            self._prefix_method = 'by_python'
            self._prefix_python_file = self._args_dict["python"]
            try:
                self._prefix_python_module = load_module(self._prefix_python_file)
            except:
                print "Error loading python code from {0:s}".format(self._prefix_python_file)
                return False
        return True
    
    def init(self, args):
        """Initialize the histogram"""
        if not self._parse_args(args):
            return False
        return True
    
    def get_prefix_by_sep(self, file_info):
        
        file_parts = re.split("/", file_info.name)
        name_part = file_parts[-1]
        #print "Processing Name: {0:s}  from {1:s}".format(name_part, file_info.name)
        index = name_part.rfind(self._prefix_sep)
        
        if index != -1:
            return name_part[:index]
        return name_part
    
    def add_file(self, file_info):
        """ Add a file to the histogram """
        
        name_prefix = "-----"
        if self._prefix_method == "by_sep":
            name_prefix = self.get_prefix_by_sep(file_info)
        elif self._prefix_method == "by_python" :
            name_prefix = self._prefix_python_module.get_file_prefix(file_info.name)
        else:
            print "Error no such method: [{0:s}]".format(self._prefix_method)
            sys.exit(1)
            
        self.update_item_counts(self._prefix_dict, "prefix", name_prefix, file_info)
        
                
    def to_str(self):
        """ Return the histogram as a printable string """
      
        hist_str = ""
        try:
            # This will create a new file or **overwrite an existing file**.
            f = open("/tmp/prefix_hist_results.txt", "w")

            if self._print_header:
                hist_str = "%-20s %10s %15s %15s\n" % ("prefix", "count", "size(MB)", "blocks(MB)")
            #print "TTTTT %s"% self._sort_type    
            if self._sort_type == "name":
                sorted_suff_items = sorted(self._prefix_dict.values(), key=lambda (v): v["prefix"])
            elif self._sort_type == "size":
                sorted_suff_items = sorted(self._prefix_dict.values(), key=lambda (v): v["total_size"])
            elif self._sort_type == "files":
                sorted_suff_items = sorted(self._prefix_dict.values(), key=lambda (v): v["count"])

            #print sorted_suff_items
            total_files = 0
            total_mb = 0
            total_blocks_mb = 0
            for suff_item in sorted_suff_items:
            
                suff_count = suff_item['count']
                suff_size_mb = suff_item['total_size'] / (1024.0 * 1024.0)
                suff_blocks_mb = (suff_item['total_blocks'] * 512)/ (1024.0 * 1024.0)
                
                total_files += suff_count
                total_mb += suff_size_mb
                total_blocks_mb += suff_blocks_mb
        
                hist_str += "%-20s %10d %15.1f %15.1f\n" % (suff_item["prefix"], suff_count, suff_size_mb, suff_blocks_mb)
                f.write("%s %d %f %f\n" % (suff_item["prefix"], suff_count, suff_size_mb, suff_blocks_mb)) # Write a string to a file
            f.close()
        
        except IOError:
            print "IO Error"
            pass
        
        # Printing the header
        if self._print_header:
            hist_str += "\n"
            hist_str += "%-20s %10d %15.1f %15.1f\n" % ("Total", total_files, total_mb, total_blocks_mb)
            
        
        return hist_str
        
        
    def save_to_file(self, file_name):
        """ Save the histogram to a file """
