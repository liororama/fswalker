"""FS Query main class

"""

# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import time
import stat
import re
import string
import logging

import FSFileInfo
from FSFileInfo import FSFileInfo

import FSInfoStorage
import TimeExecution

import HistBase
import SuffixHist
import DUHist
import SizeHist
import TimeOfLastModificationHist
import UsersHist
import PrefixHist
import HistDisplay

class FSQuery:
    """FSQuery perform file system walking controlled by several parameters."""
    
    _verbose = 0
    _show_title = False
    _err_msg = ""
    
    def __init__(self, fsstore, output_dir):
        self.output_dir = output_dir
        self._fsstore = fsstore
        self._sql_filter = ""
        self._logger = logging.getLogger("fsquery")
        self._show_progress = False
        self._progress_callback = None
        
    def set_verbose(self, v):
        self._verbose = v
        self._fsstore.set_verbose(self._verbose)
        
    def set_sql_filter(self, sql_filter):
        self._sql_filter = sql_filter
    
    @property
    def show_title(self):
        """ Show title """
        return self._show_title
    @show_title.setter
    def show_title(self, v):
        self._show_title = v
    
    def set_progrss_callback(self, progress_callback):
        self._progress_callback = progress_callback
        
    @property
    def show_progress(self):
        """ Show progress """
        return self._show_progress
   
    @show_progress.setter
    def show_progress(self, v):
        self._show_progress = v
        print "Set show progress:", self._show_progress
    @property
    def error(self):
        """Get error message"""
        return self._err_msg
        
    def count_files(self):
        """ Counting the number of lines in the file """
        
        res = self._fsstore.sql_execute("select count(id) from %s" % (self._fsstore.tbl_name()))
        return res[0][0]
        
            
    def info_str(self):
        """Generate information about the db"""
        self._logger.debug_m("Getting info on walk")
        walk_info = self._fsstore.get_walk_info()
        return str(walk_info)
    
    def errors_str(self):
        """Generate information about errors enountered during walk"""
        self._logger.debug_m("Getting info on walk errors")
       
        s = ""
        walk_errors = self._fsstore.get_walk_errors()
        for we in walk_errors:
            s += str(we) + "\n"
        
        return s
    
    
    def users_files(self):
        """Calculate the number of files (and space) each user has"""
    
    def direct_sql(self, sql_str):
        """Run direct sql query only"""
        if self._verbose : print "Running sql [%s]" % (sql_str)
        res = self._fsstore.sql_execute_no_fetch(sql_str)
        while True:
      
            row = self._fsstore.fetchone()
            #if self._verbose : print "Row:", row
            if row == None:
                break
            print ", ".join(map(str, row))
            #row[0], row[1], row[2]
        
    def set_top_dir(self, top_dir):
        """ Setting the sql filter to select only files below top_dir"""
        dir_files_filter = "name like '%s%%'" %(top_dir)
        if self._sql_filter:
            self._sql_filter = dir_files_filter + " and " + self._sql_filter
        else:
            self._sql_filter = dir_files_filter
      
    def fs_db_walk(self, func, data = None):
        """Perform a walk over the db using the provided func and data.
           for each file (line) in the db call func(file_info, data) """
        
        # Performing the sql query
        get_rows_q = "select * from %s " % (self._fsstore.tbl_name())
        if self._sql_filter:
            get_rows_q += " where %s" % (self._sql_filter)
        
        if self._verbose : print "fs_db_walk sql: ", get_rows_q
        
        res = self._fsstore.sql_execute_no_fetch(get_rows_q)
        
           
        res = True   
        fi = FSFileInfo("nonexistent")
        
        t = TimeExecution.TimeExecution()

        files_processed = 0
        
        # Lopping over the files
        while  res == True:  
            t.enter()
            # Getting many rows at once for better performance
            row_list = self._fsstore.fetchmany()
            t.exit()
            if not row_list :
                break
            
            files_processed += len(row_list)
                
            #print "LEN: {0} in time {1}".format(len(row_list), t.wall_time);
            #if self._verbose : print "Row: ", row
            
            # Iterating over the rows
            t.enter()
            for row in row_list:
                #fi.from_db_row(row)
                #if self._verbose : print "FileInfo: ", fi
                if data:
 #                   res = func(fi, data)
                    res = func(row, data)
                else:
#                    res = func(fi)
                    res = func(row)
                
                if res != True:
                    if self._verbose : print "fs_db_wal: func ret != True - stopping iter" 
                    return
            t.exit()
            
            self._logger.debug_b("Processed in {0} seconds".format(t.wall_time))
            if self.show_progress:
                self._progress_callback(files_processed, 0)
            

    def _size_hist_helper(self, file_info, data):
        """ The histogram helper func: beeing called for every file"""
        #if self._verbose : print "Doing file %s" % (file_info.name)    
        data['total_files'] += 1
        data['total_size'] += file_info.st_size
        data['total_blocks'] += file_info.st_blocks
        
        data['check_file'](file_info)
        return True
    
    
    def hist(self, hist_type, hist_args):
        """Calculating hitogram of files"""

        #hist = HistBase.HistBase()
        if hist_type == "suffix" :
            hist = SuffixHist.SuffixHist()
        elif hist_type == "prefix" :
            hist = PrefixHist.PrefixHist()
        elif hist_type == "users" :
            hist = UsersHist.UsersHist()
        elif hist_type == "size" :
            hist = SizeHist.SizeHist()
        elif hist_type == "mod-time" :
            walk_info = self._fsstore.get_walk_info()

            hist = TimeOfLastModificationHist.TimeOfLastModificationHist(curr_time=walk_info.start_timestamp)
        else:
            print "No such histogram: [%s]" % hist_type
            return False

            
        if not hist.init(hist_args) :
            return False
        
        self.fs_db_walk(hist.add_file, None)
        hist.finalize()
        return hist        

    def _du_helper(self, row, data):
        """ The callback used to get each file info """
        data['total_files'] += 1
        data['total_size'] += row[FSFileInfo.st_size_index ]
        data['total_blocks'] += row[FSFileInfo.st_blocks_index] 
        
        return True

 
    def du_on_subdirs(self, directory, count_top_dir, args_str):
    
        parts = directory.strip('/').split('/')
        self._logger.debug("Got %d parts " % (len(parts)))
        self._logger.debug(str(parts))
        
        # Generating the du histogram object
        du_hist = DUHist.DUHist()
        
        if not du_hist.init(args_str) :
            self._err_msg = du_hist.get_error()
            return False
        
        du_hist.base_dir_depth = len(parts)
        if count_top_dir:
            du_hist.base_dir_depth -=  1

        self._logger.debug("Starting walk")        
        self.fs_db_walk(du_hist.add_file, None)
        du_hist.finalize()
        return du_hist
        
    def du(self, query_args):
        """Calculating du on provided subdirectories """
       
        arg_list = re.split(",", query_args)
        du_dir = arg_list.pop(0)
        
        self._logger.debug_b("DU args: [{0}]".format(query_args))
        self._logger.debug("Du on: {0}".format(du_dir))
        
        # Setting sql_filter to get only files under the du_dir
        dir_files_filter = "name like '%s%%'" %(du_dir)
        if self._sql_filter:
            self._sql_filter = dir_files_filter + " and " + self._sql_filter
        else:
            self._sql_filter = dir_files_filter
        self._logger.debug("sql_filter [{0}]".format(self._sql_filter))
            
        # Running the du on the subdirs of the given directory presenting the subdir
        # sizes
        count_top_dir = False       
        if du_dir.endswith("/") :
            count_top_dir = False
        else:
            count_top_dir = True
        
        return self.du_on_subdirs(du_dir, count_top_dir, ",".join(arg_list))
        
    #---------------------------------------------------------    
    def list_dirs(self, query_args):
        
        res = self.direct_sql("select name from %s where file_type='dir'" % (self._fsstore.tbl_name()))
        