# Copyright (c) 2013, Lior Amar (liororama@gmail.com)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of Lior Amar nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Lior Amar BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""FS Walk main class

"""
import errno
import sys
import os
import signal
import time
import stat
import re
import datetime

import TimeExecution

import FSFileInfo
import FSWalkInfo
import FSInfoStorage


class FSWalk:
    """FSWalk perform file system walking controlled by several parameters."""

    _scan_status = "Not Running"    
    _verbose = False
    _one_line_progress = False # Print to tty one line progress string
    _status_line_len = 1
        
    _delay_files = 0  # The number of files to scan before inserting a delay
    _delay_milli = 0  # Delay in milliseconds after scanning _delay_files files
    _progres_file = "fs_walk.progress" # Name of progress file to use 
    _files_progress_update_limit = 1000 # Number of files scanned which will trigger progress update
        
    _rate_file = "fs_walk.rate"
    
    _total_files = 0
    _total_dirs  = 0
    _curr_dir_depth = 0
    _max_dir_depth = 0
    _deleted_files = 0
    _permission_problems_files = 0
    _permission_problem_log = []
    _other_error_files = 0    
    
    _walk_info = FSWalkInfo.FSWalkInfo()
    
    # Only for komodo the detect the type of the variable
    _fsstore = FSInfoStorage.FSInfoStorage("none")
    _top_dirs_count = 0
    _curr_top_dirs_count = 0
    
    # Counting users info on the go to produce the users table after a
    # successful run
    _users_dict = {}
    
    def __init__(self, scan_dir, output_dir, db_obj):
        self.scan_dir = scan_dir
        self.output_dir = output_dir
        self._fsstore = db_obj
        
    def set_verbose(self, v):
        self._verbose = v
        self._fsstore.set_verbose(self._verbose)
    
    def set_one_line_progress(self, v):
        self._one_line_progress = v
        
    def set_progress_update_limit(self, files_num):
        if files_num > 0:
            self._files_progress_update_limit = files_num
        
    def set_delay_milli(self, milli):
        self._delay_milli = milli

    def set_delay_files(self, files):
        self._delay_files = files
        
    
    def count_file(self, full_name, stat_val):
        """ Counting general info about file (for this module only """
        mode = stat_val.st_mode
            
        self._total_files +=1 
        if stat.S_ISDIR(mode):
            self._total_dirs +=1
    
        #print "Counting file ", full_name
        #print "Size:", stat_val.st_size
        
        self._walk_info.total_size_mb += stat_val.st_size / (1024.0*1024.0)
    
    def count_users(self, file_info):
        
        if not file_info.st_uid in self._users_dict:
            self._users_dict[file_info.st_uid] = {
                "uid"   :      file_info.st_uid,
                "count" :      0,
                "dir_count" :  0,
                "total_size":  0,
            }
            
        self._users_dict[file_info.st_uid]["count"] += 1
        if stat.S_ISDIR(file_info.st_mode):
            self._users_dict[file_info.st_uid]["dir_count"] += 1
                
        self._users_dict[file_info.st_uid]["total_size"] += file_info.st_size
        
    
    def get_status_line_str(self):
        """Generate a single line string containing the status of the scan"""
        return "%s : [top-dirs: %d/%d] [f: %d d: %d] [depth: %d:%d]  [err: p %d d %d]" \
                % ( self._scan_status,
                    self._curr_top_dirs_count, self._top_dirs_count,
                    self._total_files, self._total_dirs,
                    self._curr_dir_depth, self._max_dir_depth,
                    len(self._permission_problem_log), self._deleted_files)
    
    def get_summary_str(self):
        s  = "Summary of scan:\n"
        s += "-----------------\n"
        s += str(self._walk_info)
        s += "\n"
        s += "Error summary:\n"
        s += "--------------\n"
        s += "  Permissions:   {0:d}\n".format(len(self._permission_problem_log))
        s += "  Deleted:       {0:d}\n".format(self._deleted_files)
        s += "  Other:         {0:d}\n".format(self._other_error_files)
        
        return s
    
    def update_progress(self, force=0):
        """Update progress information"""
    
        if not force  and self._total_files % self._files_progress_update_limit != 0:
            return
        
        status_line = self.get_status_line_str()
        if self._verbose:
            print status_line
        elif self._one_line_progress:
            sys.stdout.write("\r" + (' ' * self._status_line_len))
            sys.stdout.write("\r" + status_line )
            sys.stdout.flush()
            self._status_line_len = len(status_line)
            
        progress_file = os.path.join(self.output_dir, self._progres_file)
        progress_fh = open(progress_file, "w")
        progress_fh.write(status_line)
        progress_fh.close()


    
    def do_sleep(self):
        if self._delay_files == 0:
            return
        if self._total_files % self._delay_files == 0:
            sleep_in_sec = self._delay_milli / 1000.0
            if(self._verbose) : print "injecting sleep of %d millisec (%f)" %(self._delay_milli, sleep_in_sec)
            time.sleep(sleep_in_sec)
    
    def add_file_to_db(self, file_info):
        """Adding a single file to the db object - it is not necesarliy added to a stable
           storage at this time
        """
        self._fsstore.add_file(file_info)
        
    def reread_rate_params_file(self):
        """ Rereading a file with rate parameters
            The file is a one liner with the following format:
            files-to-process-before sleep, sleep-time-sec
        """
        rate_file = os.path.join(self.output_dir, self._rate_file)
        
        if os.path.isfile(rate_file):
            if self._verbose : print "Detected rate file"
        else:
            if self._verbose : print "No Rate file [%s]" %(rate_file)
            return 0
        
        # We have a rate file ... reading it
        rf = open(rate_file, 'r')
        line = rf.readline()
        print "Line:", line
        rate_params_list = re.split("\s*,\s*", line)
        if len(rate_params_list) < 2 :
            print "Rate file is in wrong format: less than 2 args"
            return 0
        files_to_process = int(rate_params_list[0])
        sleep_time = int(rate_params_list[1])
        
        if self._verbose : print "Rate params files:[%d] sleep:[%d]" % (files_to_process, sleep_time)
        self._delay_files = files_to_process
        self._delay_milli = sleep_time
        
        
    def _sig_handler(self, signum, frame):
        """ Singal handler that will reread the rate file"""
        if self._verbose : print "Got signal: %d" % (signum)
        self.reread_rate_params_file()
    
    
    def get_users_info_str(self):
        
        s = ""
        
        s += "%-20s %10s %10s %10s\n" % ("User", "files", "dirs", "size")
        for u in sorted(self._users_dict.values(), key=lambda (v): v["uid"] ):
            sz_mb = u["total_size"] / (1024.0*1024.0)
            s+= "%-20s %10d %10d %10.1f\n" % (u["uid"], u["count"], u["dir_count"], sz_mb )
    
        return s
    
    def walk(self):
        """ Perform walk on the filesystem """
        
        if self._verbose : print "Verbose %d" % self._verbose
        
        if not os.path.isdir(self.scan_dir):
            print "Error : [%s] is not a directory" % (self.scan_dir)
            return False
      
        # Taking starting time
        self._walk_info.start_date = datetime.datetime.now()
        self._walk_info.start_timestamp = int(time.time())
        
        # Start time measurment
        t = TimeExecution.TimeExecution()
        t.enter()
        
        self._scan_status = "Active"
        self.update_top_dirs_count(self.scan_dir)
        
        # Installing the sigusr1 handler
        signal.signal(signal.SIGUSR1, self._sig_handler)
        self._walktree_rec(self.scan_dir)
        
        self._fsstore.store_files()
        
        t.exit()
        
        
        self._walk_info.duration_sec = t.wall_time
        self._walk_info.directory = self.scan_dir
        self._walk_info.max_depth = self._max_dir_depth
        self._walk_info.total_dirs = self._total_dirs
        self._walk_info.total_files = self._total_files
        #self._walk_info.total_size_mb = 555
        
        self._fsstore.store_walk_info(self._walk_info)
        self._scan_status = "Done"
        self.update_progress(1)
        
        print  "" 
        if not self._one_line_progress:
            status_line = self.get_status_line_str()
            print status_line
    
        print
        print self.get_summary_str()
        #self.print_users_info()
       
    
    def update_top_dirs_count(self, top):
        file_list = os.listdir(top)

        for f in file_list:
            pathname = os.path.join(top, f)
            stat_val = os.lstat(pathname)
            
            mode = stat_val.st_mode
            if stat.S_ISDIR(mode):
                self._top_dirs_count += 1
        
        
    def _walktree_rec(self, top):
        '''recursively descend the directory tree rooted at top,
        calling the callback function for each regular file'''

        file_list = []
        try:
            file_list = os.listdir(top)
        except OSError, e:
            if e.errno == errno.EACCES:
                self._permission_problem_log.append(top)
            elif e.errno == errno.ENOENT:
                self._deleted_files += 1
        
            walk_error = FSWalkInfo.FSWalkErrors()
            walk_error.file_path = top
            walk_error.errno = e.errno
            self._fsstore.store_walk_error(walk_error)
        except Exception, e:
            self._other_error_files += 1
        
        
        # Iterating over the files in the top directory
        for f in file_list:
            pathname = os.path.join(top, f)
            unicode_pathname = unicode(pathname, "utf-8", "replace")
        
            try:
                stat_val = os.lstat(pathname)
            except OSError, e:
                if e.errno == errno.ENOENT:
                    if self._verbose : print "File [%s] disappeared" % pathname
                    self._deleted_files +=1
                    continue
                elif e.errno == errno.EACCES:
                    self._permission_problem_log.append(pathname)
                    continue
                else:
                    self._other_error_files += 1
                
                walk_error = FSWalkInfo.FSWalkErrors()
                walk_error.file_path = pathname
                walk_error.errno = e.errno
                self._fsstore.store_walk_error(walk_error)
            
            
            file_info = FSFileInfo.FSFileInfo(unicode_pathname)
            file_info.set_stat_info(stat_val)
            
            if self._verbose >= 2 : print "Processing file [%s]" % pathname
            self.count_file(pathname, stat_val)
            self.count_users(file_info)
            self.update_progress()
            self.add_file_to_db(file_info)
            self.do_sleep()
            
            mode = stat_val.st_mode
            if stat.S_ISDIR(mode):
                # It's a directory, recurse into it
                self._curr_dir_depth +=1
                if self._curr_dir_depth > self._max_dir_depth:
                    self._max_dir_depth = self._curr_dir_depth
                    
                self._walktree_rec(pathname)
                self._curr_dir_depth -=1
                if(self._curr_dir_depth == 0):
                    self._curr_top_dirs_count +=1
            #elif stat.S_ISREG(mode):
                # It's a file, call the callback function
                #callback(pathname)
            #else:
                # Unknown file type, print a message
            #    print 'Skipping %s' % pathname

        