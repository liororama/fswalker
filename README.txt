
FSWalk - A Filesystem crawler
-------------------------------------------------------------------

On large filesystems it is impossible to run tools such as du and obtain
results in a reasonable time. FSWalk is a file system crawler which travels
over a filesystem and provides an sqlite db containing information about each
file. The fsq utility of the fswalk project can now be used to query the db
and obtain information about the file system in a much faster way.
The intended useage it to run fswalk in a periodic manner so the sysadmin can
monitor changes in the file system and produce reports. 

Authors: Lior Amar and Gal Oren


License: Revised BDS license

Website: https://bitbucket.org/liororama/fswalker/wiki/Home


Prerequsites:
-------------
sqlalchemy
matplotlib

Quick install:
----------------
tar xvfz fswalker.tgz
cd fswaker
python setup install

Running fswalk:
---------------

fswalk --dir /big-storage-dir   -o /opt/dbs/fswalk-dbs/ -p

The line above will run fswalk scan on the directory /big-storage-dir and 
will keep the output db at /opt/dbs/fswalk-dbs/. The -p flag will 
produce a one line progress indicator printout so the rate of scan can be 
monitored


Running fsq:
------------

fsq --help will print a help screen with several examples


Reporting bugs or suggesting improvements:
---------------------------------------------
Please report issues/bugs/suggestions in the bitbucket website.



Enjoy
 
