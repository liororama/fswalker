from distutils.core import setup


setup(
    name = "fswalker",
    version="0.1.5",
    author="Lior Amar and Gal Oren",
    author_email="liororama@gmail.com",
    packages=['fswalk'],
    scripts=['bin/fswalk', 'bin/fsq'],
    license='LICENSE.txt',
    description='File System indexer and query',
    long_description = open('README.txt').read(),
    install_requires = [
        "sqlalchemy >= 0.7.0"
    ],
)
